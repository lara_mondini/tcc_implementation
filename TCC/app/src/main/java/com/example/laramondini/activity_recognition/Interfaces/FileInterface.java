package com.example.laramondini.activity_recognition.Interfaces;

import android.hardware.SensorManager;

import java.io.FileOutputStream;

public interface FileInterface {

    public boolean rawFileStart(SensorManager mSensorManager);
    public boolean rawFileStop(SensorManager mSensorManager);
    public boolean rawFileResume(SensorManager mSensorManager);
    public void preProcessFile();
    public void normalizeFile();
}
