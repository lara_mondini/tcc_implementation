package com.example.laramondini.activity_recognition.DAO;

public class DAOConnecting {
    private static final DAOConnecting ourInstance = new DAOConnecting();

    public static DAOConnecting getInstance() {
        return ourInstance;
    }

    private DAOConnecting() {
    }
}
