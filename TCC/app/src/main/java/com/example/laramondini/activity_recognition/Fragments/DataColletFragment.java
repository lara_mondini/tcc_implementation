package com.example.laramondini.activity_recognition.Fragments;

import android.app.Activity;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;

import com.example.laramondini.activity_recognition.Implementation.FileAR;
import com.example.laramondini.activity_recognition.R;

import java.io.File;
import java.util.Calendar;

public class DataColletFragment extends Fragment {

    private Button rawRecording;
    private Button pauseRaw;
    private Button exportRaw;

    private Button importProcess;
    private Button processButton;
    private Button exportProcess;

    private Button importClassification;
    private Button classificationButton;
    private Button exportClassification;
    private Button graphClassification;

    private FileAR fileAR = new FileAR();

    private Chronometer chronometer;
    private long pauseOffset;
    private boolean running;

    private SensorManager mSensorManager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSensorManager = (SensorManager) this.getActivity().getSystemService(Activity.SENSOR_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_collet, container, false);

        chronometer = view.findViewById(R.id.chronometer_raw);
        chronometer.setText("00:00:00");

        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            public void onChronometerTick(Chronometer cArg) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, getCurrentMiliSecondsOfChronometer());
                chronometer.setText(DateFormat.format("HH:mm:ss", calendar.getTime()));
            }
        });

        // primeiro card
        rawRecording = (Button) view.findViewById(R.id.button_raw_recording);
        pauseRaw = (Button) view.findViewById(R.id.button_raw_pause);
        exportRaw = (Button) view.findViewById(R.id.button_raw_export);

        rawRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean successStart = false;
                startChronometer();

                successStart = fileAR.rawFileStart(mSensorManager);
                Toast.makeText(getActivity(), "Raw recording started", Toast.LENGTH_SHORT).show();

                if (!successStart) {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                    resetChronometer();
                } else {
                    String dir = Environment.getExternalStorageDirectory() + File.separator +"ActivityRecording/";
                    Toast.makeText(getActivity(), "Saved to " + dir, Toast.LENGTH_LONG).show();
                }
            }
        });

        pauseRaw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean successPause = false;
                pauseChronometer();
                successPause = fileAR.rawFileResume(mSensorManager);
                Toast.makeText(getActivity(), "Pause recording", Toast.LENGTH_SHORT).show();
                if (!successPause) {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                    resetChronometer();
                }
            }
        });

        exportRaw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean successExport = false;
                resetChronometer();
                successExport = fileAR.rawFileStop(mSensorManager);
                Toast.makeText(getActivity(), "Export recording", Toast.LENGTH_SHORT).show();
                if (!successExport) {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //segundo card
        importProcess = (Button) view.findViewById(R.id.button_import_pre_process);
        processButton = (Button) view.findViewById(R.id.button_export_pre_process);
        exportProcess = (Button) view.findViewById(R.id.button_export_pre_process);

        importProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Import pre process", Toast.LENGTH_SHORT).show();
            }
        });

        processButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Pre process", Toast.LENGTH_SHORT).show();
            }
        });

        exportProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Export pre process", Toast.LENGTH_SHORT).show();
            }
        });

        //terceiro card
        importClassification = (Button) view.findViewById(R.id.button_import_classification);
        classificationButton = (Button) view.findViewById(R.id.button_export_classification);
        exportClassification = (Button) view.findViewById(R.id.button_export_classification);
        graphClassification = (Button) view.findViewById(R.id.button_graph_classification);

        importClassification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Import classification", Toast.LENGTH_SHORT).show();
            }
        });

        classificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "classification", Toast.LENGTH_SHORT).show();
            }
        });

        exportClassification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Export classification", Toast.LENGTH_SHORT).show();
            }
        });

        graphClassification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Graph classification", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    private void startChronometer() {
        if (!running) {
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            running = true;
        }

    }

    private void pauseChronometer() {
        if (running) {
            chronometer.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
            running = false;
        }
    }

    private void resetChronometer() {
        chronometer.setBase(SystemClock.elapsedRealtime());
        pauseOffset = 0;
    }

    private int getCurrentMiliSecondsOfChronometer() {
        int stoppedMilliseconds = 0;
        String chronoText = chronometer.getText().toString();
        String array[] = chronoText.split(":");
        if (array.length == 2) {
            stoppedMilliseconds = Integer.parseInt(array[0]) * 60 * 1000 + Integer.parseInt(array[1]) * 1000;
        } else if (array.length == 3) {
            stoppedMilliseconds =
                    Integer.parseInt(array[0]) * 60 * 60 * 1000 + Integer.parseInt(array[1]) * 60 * 1000
                            + Integer.parseInt(array[2]) * 1000;
        }
        return stoppedMilliseconds;
    }

}
