package com.example.laramondini.activity_recognition.Implementation;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import com.example.laramondini.activity_recognition.Interfaces.FileInterface;

import java.io.FileOutputStream;

public class FileAR implements FileInterface {

    private Accelerometer accelerometer = new Accelerometer();

    @Override
    public boolean rawFileStart(SensorManager mSensorManager) {
        return accelerometer.startCollectData(mSensorManager);
    }

    @Override
    public boolean rawFileResume(SensorManager mSensorManager){
        return accelerometer.resumeColletData(mSensorManager);
    }

    @Override
    public boolean rawFileStop(SensorManager mSensorManager){
        return accelerometer.stopColletData(mSensorManager);
    }

    @Override
    public void preProcessFile() {

    }

    @Override
    public void normalizeFile() {

    }


}
