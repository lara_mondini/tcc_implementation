package com.example.laramondini.activity_recognition.Implementation;

public class AccelerometerData {

    private Long X;
    private Long Y;
    private Long Z;
    private Long timestamp;

    public Long getX() {
        return X;
    }

    public void setX(Long x) {
        X = x;
    }

    public Long getY() {
        return Y;
    }

    public void setY(Long y) {
        Y = y;
    }

    public Long getZ() {
        return Z;
    }

    public void setZ(Long z) {
        Z = z;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
